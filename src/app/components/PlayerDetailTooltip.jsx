import styled from "styled-components";

const Container = styled.div`
width: 300px;
position: absolute;
top: 0;
left: 100%;
transform: translateX(-50%);
background-color: #333;
color: #fff;
padding: 10px;
border-radius: 5px;
opacity: 0.8;
`;

const Text = styled.div`
font-size: 14px;
`;
const PlayerDetailTooltip = ({ player }) => {
  return (
    <Container>
      <Text>
        <div>{player.dateBorn}</div>
        <div>{player.strHeight}</div>
        <div>{player.strWeight}</div>
        <div>{player.strDescriptionEN}</div>
      </Text>
    </Container>
  )
}

export default PlayerDetailTooltip