import React from "react";
import { useMemo, useState } from "react";
import { getPosition } from "../utils/getPosition";
import PlayerDetailTooltip from "./PlayerDetailTooltip";
import styled from "styled-components";

const TableContainer = styled.div`
position: relative;
`;

const PlayerTable = ({ players, addPlayer, caller, removePlayer }) => {

    const [showTooltip, setShowTooltip] = useState(false);
    const [hoveredPlayer, setHoveredPlayer] = useState();

    // Pattern 1: when the string contains 'a week'" ex. £10,140,000\t(£300,000 a week)
    const wagePattern1 = /£([\d,]+) a week/;
    // Pattern ex: "£110,000 Per week"
    const wagePattern2 = /£(\d{1,3}(?:,\d{3})*) Per week/;
    // Pattern ex: "£2,080,000"
    const wagePattern3 = /^£(\d{1,3}(?:,\d{3})*)$/;
    // Pattern ex: "€170K"
    const wagePattern4 = /€(\d+)/;

    const formatWeeklySalary = (strWage) => {
        if (!strWage) {
            return 100;
        } else if (wagePattern1.test(strWage)) {
            const match = strWage.match(wagePattern1);
            const parts = match[1].split(",");
            return parts[0];
        } else if (wagePattern2.test(strWage)) {
            const match = strWage.match(wagePattern2);
            const parts = match[1].split(",");
            return parts[0];
        } else if (wagePattern3.test(strWage)) {
            // 1.  Extract 2,080,000
            const match = strWage.match(wagePattern3);
            const wage = match[1].replace(/,/g, '');  //2080000
            // 2. Cut the last 000, => 2,080
            const abbreviatedWage = wage.slice(0, -3); // 2080
            // 3. Devide by 52
            return (Number(abbreviatedWage / 52)).toString();
        } else if (wagePattern4.test(strWage)) {
            const match = strWage.match(wagePattern4);
            const parts = match[1].split(",");
            return parts[0];
        }
        return null;
    }

    const handleClick = (player) => {
        addPlayer(player);
    }
    const handleRemove = (index) => {
        removePlayer(index);
    }

    const handleMouseOver = (player) => {
        setHoveredPlayer(player);
        setShowTooltip(true);
    }

    const memoizedPlayers = useMemo(() => {
        return players.map((player) => ({
            ...player,
            memoizedPosition: getPosition(player.strPosition)
        }));
    }, [players, getPosition]);


    return (
        <TableContainer>
            {showTooltip && <PlayerDetailTooltip player={hoveredPlayer} />}

            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Wage per week</th>
                        <th scope="col">Position</th>
                    </tr>
                </thead>
                <tbody>
                    {memoizedPlayers.map((player, index) => (
                        <tr
                            key={index}
                            onMouseOver={() => handleMouseOver(player)}
                            onMouseLeave={() => setShowTooltip(false)}
                        >
                            <td onClick={() => handleClick(player)}>{player.strPlayer}</td>
                            <td>{formatWeeklySalary(player.strWage)}</td>
                            <td>{player.memoizedPosition}</td>
                            <td>{caller === "OwnPlayers" && <button onClick={() => handleRemove(index)}>Remove</button>}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </TableContainer>

    )
}

export default PlayerTable