import styled from "styled-components";

const ToastContainer = styled.div`
  position: fixed;
  bottom: 20px;
  right: 20px;
  background-color: #333;
  color: #fff;
  padding: 10px 20px;
  border-radius: 5px;
  box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.2);
  transition: visibility 0.3s, opacity 0.3s;
`;
//It was hard to make Bootstrap's toast work with Next.js
//I ended up in making this component with styled-components
const Toast = ({ message, setShowToast }) => {
    const handleClose = () => {
        setShowToast(false);
    };

    return (
        <ToastContainer >
            {message}
            <button onClick={handleClose}>Close</button>
        </ToastContainer>
    );
};

export default Toast