const TeamInfo = ({ team }) => {
    return (
        <>
            <div>{team.strTeam}</div>
            <div>{team.intFormedYear}</div>
            <div>{team.strLeague}</div>
            <div>{team.strStadium}</div>
            <img src={team.strTeamBadge} alt="Team Badge" style={{ maxWidth: '100px' }} />
            <img src={team.strTeamJersey} alt="Team Badge" style={{ maxWidth: '100px' }} />
        </>
    )
}

export default TeamInfo