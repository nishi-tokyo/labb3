import React from "react";
import { convertWageToWeeklyFormat } from "../utils/convertWageToWeeklyFormat";
const TotalCost = ({ savedPlayers }) => {

    const calculateTotalCost = () => {
        if (savedPlayers) {
            let sum = 0;
            savedPlayers.forEach(player => {
                const intWage = Number(convertWageToWeeklyFormat(player.strWage));
                sum += intWage;
            });
            return sum;
        }
    }

    return (

        <div>
            Total Cost: {calculateTotalCost()}
        </div>

    )
}

export default TotalCost