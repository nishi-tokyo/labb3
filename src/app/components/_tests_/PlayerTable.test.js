import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import PlayerTable from '../PlayerTable';

jest.mock('../../utils/getPosition', () => {
    return {
        getPosition: jest.fn((strPosition) => {
            switch (strPosition) {
                case 'Wing':
                    return 'Forward';
                case 'Defensive Midfield':
                    return 'Midfield';
                case 'Centre-Back':
                    return 'Defender';
                case 'Goalkeeper':
                    return 'Goalkeeper';
                default:
                    return strPosition;
            }
        }),
    };
});

describe('PlayerTable Component', () => {
    it('renders the player table with correctly formatted wage and position', () => {
        const players = [
            { strPlayer: 'Player 1', strWage: '£10,140,000\t(£300,000 a week)', strPosition: 'Wing' },
            { strPlayer: 'Player 2', strWage: '£110,000 Per week', strPosition: 'Defensive Midfield' },
            { strPlayer: 'Player 3', strWage: '£2,080,000', strPosition: 'Centre-Back' },
            { strPlayer: 'Player 4', strWage: '€170K', strPosition: 'Goalkeeper' },
        ];

        const addPlayer = jest.fn();
        const removePlayer = jest.fn();
        const caller = 'OwnTeam';

        const { getByText } = render(<PlayerTable players={players} addPlayer={addPlayer} caller={caller} removePlayer={removePlayer} />);

        expect(getByText('Player 1')).toBeInTheDocument();
        expect(getByText('Player 2')).toBeInTheDocument();
        expect(getByText('Player 3')).toBeInTheDocument();
        expect(getByText('Player 4')).toBeInTheDocument();

        expect(getByText('Forward')).toBeInTheDocument();
        expect(getByText('Midfield')).toBeInTheDocument();
        expect(getByText('Defender')).toBeInTheDocument();
        expect(getByText('Goalkeeper')).toBeInTheDocument();

        expect(getByText('300')).toBeInTheDocument();
        expect(getByText('110')).toBeInTheDocument();
        expect(getByText('40')).toBeInTheDocument();
        expect(getByText('170')).toBeInTheDocument();
    });

    it('calls addPlayer when player name is clicked', () => {
        const players = [
            { strPlayer: 'Player 1', strWage: '£200,000 a week', strPosition: 'Forward' },
        ];
        const addPlayerMock = jest.fn();
        const removePlayerMock = jest.fn();

        const { getByText } = render(<PlayerTable players={players} addPlayer={addPlayerMock} removePlayer={removePlayerMock} caller="OwnPlayers" />);

        fireEvent.click(getByText('Player 1'));

        expect(addPlayerMock).toHaveBeenCalledWith({
            strPlayer: 'Player 1',
            strWage: '£200,000 a week',
            strPosition: 'Forward',
            memoizedPosition: 'Forward'
        });
    });

    it('Remove button is visible when used by OwnTeam component', () => {
        const players = [
            { strPlayer: 'Player 1', strWage: '£200,000 a week', strPosition: 'Forward' },
        ];
        const addPlayerMock = jest.fn();
        const removePlayerMock = jest.fn();

        const caller = 'OwnPlayers';

        const { getByText } = render(<PlayerTable players={players} addPlayer={addPlayerMock} removePlayer={removePlayerMock} caller={caller} />);
        expect(getByText('Remove')).toBeInTheDocument();
    });

    it('removePlayer is called when remove button is clicked', () => {
        const players = [
            { strPlayer: 'Player 1', strWage: '£200,000 a week', strPosition: 'Forward' },
        ];
        const addPlayerMock = jest.fn();
        const removePlayerMock = jest.fn();

        const caller = 'OwnPlayers';

        const { getByText } = render(<PlayerTable players={players} addPlayer={addPlayerMock} removePlayer={removePlayerMock} caller={caller} />);
        
        fireEvent.click(getByText('Remove'))
        expect(removePlayerMock).toHaveBeenCalledWith(0);
    });
})