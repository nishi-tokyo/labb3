import React from 'react';
import { render } from '@testing-library/react';
import TotalCost from "../TotalCost";

describe('TotalCost Component', () => {
    it('renders correctly with no savedPlayers', () => {
        const { getByText } = render(<TotalCost savedPlayers={null}/>)
        expect(getByText(/Total Cost:/)).toBeInTheDocument();
    });
    it('should correctly calculate the total cost for different wage formats"', () => {
        const savedPlayers = [
            { strWage: '' }, //Check if empty string returns 100. 
            { strWage: '£10,140,000\t(£300,000 a week)' },
            { strWage: '£2,910,000 (£55,962 a week)' },
            { strWage: '£200,000 a week' },
            { strWage: '£110,000 Per week' },
            { strWage: '£2,080,000' },
            { strWage: '€170K' }
        ];

        const { getByText } = render(<TotalCost savedPlayers={savedPlayers} />)
        expect(getByText('Total Cost: 975')).toBeInTheDocument();
    })
})