import PlayerTable from "./PlayerTable";
import TotalCost from "./TotalCost";

const OwnTeam = ({ savedPlayers, removePlayer }) => {

    return (
        <>
            <div>OwnTeam</div>
            <TotalCost savedPlayers={savedPlayers} />
            <PlayerTable players={savedPlayers} caller="OwnPlayers" removePlayer={removePlayer} />
        </>
    )
}

/**
 * 11 players
 * 1 goalkeeper
 * 4 defenders
 * 4 midfielders
 * 2 strikers
 * 
 * 
 * winger -> striker
 * wing -> striker
 * forward -> striker
 * back -> defender
 * midfield -> midfielder
 * goalkeeper -> goalkeeper
 * limit => 130000
 */

export default OwnTeam