'use client'
import { useEffect, useState } from "react";
import SearchBar from "../components/SearchBar";
import axios from "axios";
import PlayerTable from "../components/PlayerTable";
import OwnTeam from "../components/OwnTeam";
import { getPosition } from "../utils/getPosition";

import Toast from "../components/Toast";
import TeamInfo from "../components/TeamInfo";
import { convertWageToWeeklyFormat } from "../utils/convertWageToWeeklyFormat";


const TEAM_URL = "https://www.thesportsdb.com/api/v1/json/3/searchteams.php?t="
//With this api currently it seems that there are no other teams available than Arsenal.
const PLAYERS_URL = "https://www.thesportsdb.com/api/v1/json/3/searchplayers.php?t="

const Teams = () => {
    const [team, setTeam] = useState();
    const [players, setPlayers] = useState();
    const [savedPlayers, setSavedPlayers] = useState([]);
    const [showToast, setShowToast] = useState(false);
    const [toastMessage, setToastMessage] = useState("");

    const fetchTeam = async (teamQuery) => {
        const response = await axios.get(TEAM_URL + teamQuery);
        setTeam(response?.data?.teams[0])
    }

    const fetchPlayers = async (teamQuery) => {
        const response = await axios.get(PLAYERS_URL + teamQuery);
        setPlayers(response.data.player);
    }

    const handleSearch = (teamQuery) => {
        fetchTeam(teamQuery);
        fetchPlayers(teamQuery);
    };

    const removePlayer = (index) => {
        const newSavedPlayers = savedPlayers.filter((player, i) => i !== index);
        setSavedPlayers(newSavedPlayers);
    }

    const addPlayer = (player) => {
        console.log(savedPlayers);
        const positionCurrentPlayer = getPosition(player.strPosition);
        const positions = savedPlayers.map(player => player.strPosition);
        //Checker for whether the selection goes along with 4-4-2 regulation.
        const positionCounts = {};
        for(const position of positions){
            if(positionCounts[position]){
                positionCounts[position]++;
            } else {
                positionCounts[position] = 1;
            }
        }

        // No same player allowed
        if (savedPlayers.map(player => player.strPlayer).includes(player.strPlayer)) {
            setToastMessage("You have already the selected player in your team."),
            setShowToast(true);
            return;
        }

        // Not more than 11 players allowed
        if (savedPlayers.length === 11) {
            setToastMessage("You have already 11 players.")
            setShowToast(true);
            return;
        }

        //TotalCost not more than 2000. 
        if (isTotalCostOver2000()) {
            setToastMessage("The total cost cannot exceed 2000.")
            setShowToast(true);
            return;
        }

        /* Comments this out as the external api is not fully working. 
           This is one of the most important conditions but pointless because of the sport db api.
        if (savedPlayers.filter(p => p.strTeam === player.strTeam).length > 3) {
            setToastMessage("You cannot have more than 3 players from one team.");
            setShowToast(true);
            return;
        }*/
        

        else {
            if(positionCurrentPlayer === "Striker" && positionCounts.Striker === 2){
                setToastMessage("You cannot have more than two strikers.");
                setShowToast(true);
                return;       
            } else if(positionCurrentPlayer === "Midfielder" && positionCounts.Midfielder === 4){
                setToastMessage("You cannot have more than four midfielders.");
                setShowToast(true);
                return;
            } else if(positionCurrentPlayer === "Defender" && positionCounts.Defender === 4){
                setToastMessage("You cannot have more than four defenders.");
                setShowToast(true);
                return;
            } else if(positionCurrentPlayer === "Goalkeeper" && positionCounts.Goalkeeper === 1){
                setToastMessage("You cannot have more than one goalkeeper.");
                setShowToast(true);
                return;
            }
            
            player.strPosition = getPosition(player.strPosition);
            const newSavedPlayers = [...savedPlayers, player];
            setSavedPlayers(newSavedPlayers);
        }
    }

    const isTotalCostOver2000 = () => {
        let sum = 0;
            savedPlayers.forEach(player => {
                const intWage = Number(convertWageToWeeklyFormat(player.strWage));
                sum += intWage;
            });
        return sum >= 2000;
    }

    return (
        <>
            {showToast && <Toast message={toastMessage} setShowToast={setShowToast} />}
            <div className="container">
                <div className="row">
                    <div className="col-6">
                        <SearchBar handleSearch={handleSearch} type="team" />
                        {team && <TeamInfo team={team} />}
                        {players && <PlayerTable players={players} addPlayer={addPlayer} />}
                    </div>
                    <div className="col-6">
                        <OwnTeam savedPlayers={savedPlayers} removePlayer={removePlayer} />
                    </div>

                </div>
            </div>
        </>

    )
}

export default Teams