import Link from "next/link"


const Home = () => {
  return (
    <>
      <div>Welcome to the Home Page</div>
      <div>
        <Link href="/teams">Teams</Link>
      </div>
    </>
  )
}

export default Home
