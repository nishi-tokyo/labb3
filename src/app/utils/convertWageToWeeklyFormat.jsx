export const convertWageToWeeklyFormat = (playerWage) => {
    if (!playerWage) {
        return "100";
    }
    // Pattern 1: when the string contains 'a week'"
    const wagePattern1 = /£([\d,]+) a week/;
    // Pattern ex: "£110,000 Per week"
    const wagePattern4 = /£(\d{1,3}(?:,\d{3})*) Per week/;
    // Pattern ex: "£2,080,000"
    const wagePattern5 = /^£(\d{1,3}(?:,\d{3})*)$/;
    // Pattern ex: "€170K"
    const wagePattern6 = /€(\d+)/;

    if (wagePattern1.test(playerWage)) {
        const match = playerWage.match(wagePattern1);
        const wageWithoutComma = match[1].replace(/,/g, '');
        return wageWithoutComma.slice(0, -3);
    } else if (wagePattern4.test(playerWage)) {
        const match = playerWage.match(wagePattern4);
        const wageWithoutComma = match[1].replace(/,/g, '');
        return wageWithoutComma.slice(0, -3);
    } else if (wagePattern5.test(playerWage)) {
        console.log(playerWage);
        // 1.  Extract 2,080,000
        const match = playerWage.match(wagePattern5);
        const wage = match[1].replace(/,/g, '');  //2080000
        // 2. Cut the last 000, => 2,080
        const abbreviatedWage = wage.slice(0, -3); // 2080
        // 3. parse number and devide by 52
        return Number(abbreviatedWage / 52);
    } else if (wagePattern6.test(playerWage)) {
        const match = playerWage.match(wagePattern6);
        return match[1];
    }

    return 0;
}