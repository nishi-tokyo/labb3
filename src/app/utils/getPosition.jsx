export const getPosition = (strPosition) => {
    const lower = strPosition.toLowerCase();

    if (lower.includes("winger") || 
        lower.includes("wing") || 
        lower.includes("forward") ||
        lower.includes("striker")
    ) {
        return "Striker";
    } 
    if (lower.includes("midfield") || lower.includes("midfielder")) {
        return "Midfielder";
    } 
    if (lower.includes("back") || lower.includes("defender")) {
        return "Defender";
    } 
    if (lower.includes("goalkeeper") || lower.includes("goalkeeper")) {
        return "Goalkeeper";
    }

    return "Unknown";
};